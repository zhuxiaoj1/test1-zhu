// Test 1 Danilo Zhu 1943382
package tests;

import org.junit.jupiter.api.Test;
import cars.Car;

import static org.junit.jupiter.api.Assertions.*;

class CarTests {

    @Test
        // This test tests the constructor and its accessors.
    void carAccessTests() {
        Car testCar = new Car(50);

        assertFalse(testCar.getLocation() < 0); // Checking if the speed is not negative.
        assertEquals(0, testCar.getLocation(), "Base location should be 0.");
        assertEquals(50, testCar.getSpeed(), "Car speed should equal 50.");
    }


    @Test
        // This test tests if the current speed is added to the location of the car.
    void moveRightTest() {
        Car testCar = new Car(40);
        // Location is 0 at this point but after using the method it should be 40.
        testCar.moveRight();

        assertEquals(40, testCar.getLocation(), "Location should be 40.");
    }

    @Test
        // This test tests if the current speed is subtracted from the location of the car.
    void moveLeftTest() {
        Car testCar = new Car(25);
        // Location is 0 at this point but after using the method two times it should be -50.
        testCar.moveLeft();
        testCar.moveLeft();

        assertEquals(-50, testCar.getLocation(), "Location should be -50.");
    }

    @Test
        // This method tests if the Car object's speed increases after using the accelerate method.
    void accelerateTest() {
        Car testCar = new Car(75);
        // This car has a base speed of 75, but through the following loop, it should increase to 100.
        for (int i = 0; i < 25; i++) {
            testCar.accelerate();
        }

        assertEquals(100, testCar.getSpeed(), "Speed should equal 100.");
    }

    @Test
        // This method tests if the Car object's speed decreases after using the decelerate method.
    void decelerateTest() {
        Car testCar = new Car(37);
        // This car object has a speed of 37, which should be reduced to 0 after using the following loop.
        for (int i = 0; i < 37; i++) {
            testCar.decelerate();
        }
        assertEquals(0, testCar.getSpeed(), "Speed should equal 0.");

        // Using the decelerate method after this should not reduce the speed more.
        testCar.decelerate();
        assertEquals(0, testCar.getSpeed(), "Speed should still equal 0.");
    }
}