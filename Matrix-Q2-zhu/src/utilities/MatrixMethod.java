// Question 2 Danilo Zhu 1943382
package utilities;

public class MatrixMethod {
    // This method duplicates array elements of the arrays inside a 2 dimensional array
    public static int[][] duplicate(int[][] toBeDupl) {
        for (int i = 0; i < toBeDupl.length; i++) {
            int[] dupArr = new int[toBeDupl[i].length * 2];

            for (int j = 0; j < toBeDupl[i].length; j++) {
                dupArr[j] = toBeDupl[i][j];
            }
            for (int j = toBeDupl[i].length; j < dupArr.length; j++) {
                dupArr[j] = toBeDupl[i][j - toBeDupl[i].length];
            }

            toBeDupl[i] = dupArr;
        }
        return toBeDupl;
    }
}
