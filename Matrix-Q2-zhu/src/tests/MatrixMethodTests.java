// Question 2 Danilo Zhu 1943382
package tests;

import org.junit.jupiter.api.Test;
import utilities.MatrixMethod;
import static org.junit.jupiter.api.Assertions.*;

class MatrixMethodTests {
    @Test // This method tests if the arrays inside the 2D array get duplicated
    void duplicateTest() {
        int[][] testArr = {{1,2,3,4}, {4,5,6,7}};
        MatrixMethod.duplicate(testArr);

        assertArrayEquals(new int[] {1,2,3,4,1,2,3,4}, testArr[0], "First array position should be {1,2,3,1,2,3}");
        assertArrayEquals(new int[] {4,5,6,7,4,5,6,7}, testArr[1], "Second array position should be {4,5,6,4,5,6}");
    }

}